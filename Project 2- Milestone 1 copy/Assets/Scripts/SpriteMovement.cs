﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMovement : MonoBehaviour
{

    // Recognition that there is a Rigidbody2D with the game object.
    Rigidbody2D rb;
    // This allows the designer to modify the speed of the sprite.
    public float speed;
    // Recognition that there is a Transform with the game object.
    public Transform tf;
    // This allows the designer to modify the speed of turning left and right.
    public float turnSpeed;
    // This allows the p button to pause and unpause by setting it to a boolean.
    public bool pause = false;

    void Awake()
    {
        // Variable declaring that there is an attachment between the game object and the component of a Rigidbody2D.
        rb = GetComponent<Rigidbody2D>();
        // Variable declaring that there is an attachment between the game object and the component of a Transform.
        tf = GetComponent<Transform>();
    }

    void Update()
    {
        // This line of code sets the "w" button to move the sprite forward.
        if (Input.GetKey("w"))
        {
            tf.Translate(tf.up * speed * Time.deltaTime, Space.World);
        }
        // This line of code sets the "a" button to rotate the sprite left. 
        if (Input.GetKey("a"))
        {
            transform.Rotate(Vector3.forward * turnSpeed);
        }
        // This line of code sets the "d" button to rotate the sprite right.
        if (Input.GetKey("d"))
        {
            transform.Rotate(Vector3.back * turnSpeed);
        }
        // This line of code sets the space button to reset the position to the original position.
        if (Input.GetKeyDown(KeyCode.Space))
        {
            transform.localPosition = new Vector3(0, 0, 0);
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
        // This if else statement sets the p button to pause and unpause the game.
        if (Input.GetKeyDown("p"))
        {
            if (pause)
                Time.timeScale = 1;
            else
                Time.timeScale = 0;

            pause = !pause;
        }
    }
}